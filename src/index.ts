import { CssQueryApi } from './lib/CssQueryApi'


if(!module.parent) {
	console.error(
		'This package is not directly runnable.\n' +
		'Please refer to the README.'
	)
	process.exit(0)
}

module.exports = new CssQueryApi()

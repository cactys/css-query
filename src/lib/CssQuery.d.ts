import { IStylesheetQueryOptions } from './interfaces/IStylesheetQueryOptions'


export class CssQuery {
	public static loadStylesheetUrl(url: string): Promise<void>
	public loadStylesheetFile(file: string): Promise<void>
	public loadStylesheet(css: string): Promise<void>

	public query(options: IStylesheetQueryOptions)
	public query(selector: string, property?: string)
}

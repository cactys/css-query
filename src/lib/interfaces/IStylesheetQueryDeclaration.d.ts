export interface IStylesheetQueryDeclaration {
	property: string
	value: string
	important: boolean
}

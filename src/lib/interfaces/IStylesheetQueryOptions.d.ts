export interface IStylesheetQueryOptions {
	selector?: string
	property?: string
	raw?: boolean
}

import { CssQuery } from './CssQuery'


export class CssQueryApi {
	public forUrl(url: string): Promise<CssQuery> {
		const query: CssQuery = new CssQuery
		return query.loadStylesheetUrl(url)
			.then(() => {
				return query
			})
	}

	public forFile(file: string): Promise<CssQuery> {
		const query: CssQuery = new CssQuery
		return query.loadStylesheetFile(file)
			.then(() => {
				return query
			})
	}

	public forInline(css: string): Promise<CssQuery> {
		const query: CssQuery = new CssQuery
		return query.loadStylesheet(css)
			.then(() => {
				return query
			})
	}
}

import { IStylesheetQueryOptions } from './interfaces/IStylesheetQueryOptions'
import { IStylesheetQueryDeclaration } from './interfaces/IStylesheetQueryDeclaration'
import * as path from 'path'


const cssParse    = require('css').parse,
	  fetchUrl    = require('fetch').fetchUrl,
	  fs          = require('fs'),
	  specificity = require('specificity')

export class CssQuery {
	public stylesheetInfo: Array<any>

	public loadStylesheetUrl(url: string): Promise<void> {
		return new Promise((fulfill: (buffer: Buffer) => void, reject) => {
			fetchUrl(url, (err, data, body) => {
				if(err) return reject(err)

				fulfill(body)
			})
		})
			.then(buffer => {
				return this.loadStylesheet(buffer.toString())
			}, err => {
				throw err
			})
	}
	public loadStylesheetFile(file: string): Promise<void> {
		return new Promise((fulfill: (buffer: Buffer) => void, reject) => {
			fs.readFile(path.join(path.isAbsolute(file) ? '' : process.cwd(), file), (err, data) => {
				if(err) return reject(err)

				fulfill(data)
			})
		})
			.then(buffer => {
				return this.loadStylesheet(buffer.toString())
			}, err => {
				throw err
			})
	}
	public loadStylesheet(css: string): Promise<void> {
		return new Promise((fulfill, reject) => {
			try {
				const parsedStylesheet = cssParse(css),
					  rules = parsedStylesheet.stylesheet.rules

				let _rules: any = []

				/**
				 * Transforming th css-parse tree into a more suitable tree
				 */
				for(let rule of rules) {
					const declarations: Array<IStylesheetQueryDeclaration> = []
					for(let declaration of rule.declarations) {
						const declarationObject: IStylesheetQueryDeclaration = {
							property: declaration.property,
							value: declaration.value.replace(/\s*!important\s*/g, ''),
							important: declaration.value.indexOf('!important') > -1
						}
						declarations.push(declarationObject)
					}

					for(let selector of rule.selectors) {
						_rules.push({
							selector: selector,
							declarations: declarations
						})
					}
				}

				/**
				 * Sorting by specificity
				 */
				_rules.sort((a, b) => {
					return specificity.compare(a.selector, b.selector)
				})

				const _rulesFlat = {}

				/**
				 * Merging properties of identical selector ruleblocks
				 */
				for(let rule of _rules) {
					_rulesFlat[rule.selector] = (_rulesFlat[rule.selector] || []).concat(rule.declarations)
				}

				_rules = _rulesFlat


				/**
				 * Flattening duplicate properties (by comparing importantness)
				 */
				for(let selector in _rules) {
					if(!_rules.hasOwnProperty(selector)) continue

					const declarations = _rules[selector]
					const cleanDeclarations = {}

					for(let declarationIndex in declarations) {
						if(!declarations.hasOwnProperty(declarationIndex)) continue

						let declaration = declarations[declarationIndex]
						if((cleanDeclarations[declaration.property] == undefined) || (cleanDeclarations[declaration.property].important == declaration.important) || declaration.important) {
							cleanDeclarations[declaration.property] = declaration
						}
					}

					_rules[selector] = cleanDeclarations
				}

				this.stylesheetInfo = _rules

				fulfill()
			} catch(e) {
				reject(e)
			}
		})
	}

	public query(options: IStylesheetQueryOptions): IStylesheetQueryDeclaration | string {
		const defaults: IStylesheetQueryOptions = {
			selector: undefined,
			property: undefined,
			raw: false
		}

		for(let option in defaults) {
			options[option] = options[option] || defaults[option]
		}

		if(!options.selector) {
			throw 'No selector given!'
		}

		if(!this.stylesheetInfo) {
			throw 'No stylesheet loaded to parse!'
		}

		const rule = this.stylesheetInfo[options.selector]
		if(!rule) {
			throw 'Unknown selector!'
		}

		if(options.raw) {
			for(let declarationName in rule) {
				if(!rule.hasOwnProperty(declarationName)) continue
				rule[declarationName] = rule[declarationName].value + (rule[declarationName].important ? ' !important' : '')
			}
		}

		if(options.property) {
			if(!rule[options.property]) {
				return undefined
			}
			return rule[options.property]
		}

		return rule
	}
}

# css-query [![NPM version](https://img.shields.io/npm/v/css-query.svg)](https://www.npmjs.com/package/css-query)
> A simple parser that allows querying of CSS values in a stylesheet.

## Why?
I needed to parse values off a CSS stylesheet in a personal project, so
I created this because I didn't find anything that did this.

## Example
**Warning:**  
<u>Currently</u> it's only possible to query for a selector that's
actually present in the CSS since there isn't any matching functionality
present yet.

That's why a random selector like `html body #foo.active:hover` isn't
going to work if you don't specifically define a selector like that.

---

`style.css`:
```css
#foo {
	color: red;
	font-size: 20px;
}

#foo.active {
	color: green !important;
}
```

`index.js`:
```js
const cssQuery = require('css-query');

cssQuery.forFile('style.css')
	.then(query => {
		const color = query.query({
			selector: '#foo.active',
			property: 'color'
		}).value

		console.log(`The element's color is: ${color}`)
	})

```

**Output**:
```txt
The element's color is: green
```

## To do
I won't be actively working on this, but there's still some stuff that
should get added to this over time.

- Getting this to work with **@media** queries
- Generic selector matching, so that it's less like selecting the key
from a JSON and more like "I have 'span.foo', which styles do I get for
it in this stylesheet?"

## Contributing
I'll gladly appreciate any contribution on this package!

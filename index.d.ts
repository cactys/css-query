import { CssQuery } from './src/lib/CssQuery'


declare module 'css-query' {
	export function forUrl(url: string): Promise<CssQuery>
	export function forFile(file: string): Promise<CssQuery>
	export function forInline(css: string): Promise<CssQuery>
}

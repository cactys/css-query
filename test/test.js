const cssQuery = require('../index'),
	  path = require('path')

cssQuery.forFile(path.join(__dirname, 'test.css'))
	.then(query => {
		const color = query.query({
			selector: '#foo.active',
			property: 'color'
		}).value

		console.log(`The element's color is: "${color}" (expected: "green")`)
		process.exit(1 - (color === 'green'))
	})
